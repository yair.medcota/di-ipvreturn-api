export class Constants {

    static readonly DEBUG = "DEBUG";

    static readonly INFO = "INFO";

    static readonly WARN = "WARN";

    static readonly ERROR = "ERROR";

    static readonly ENV_VAR_UNDEFINED = "ENV Variables are undefined";

    static readonly EMAIL_LOGGER_SVC_NAME = "SendEmailHandler";

    static readonly EMAIL_METRICS_NAMESPACE = "F2F-CRI";
}
